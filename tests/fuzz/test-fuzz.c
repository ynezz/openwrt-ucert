#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <limits.h>

#include "cert_parse.h"

int LLVMFuzzerTestOneInput(const uint8_t *input, size_t size)
{
	uint8_t *data;
	static LIST_HEAD(certchain);

	data = malloc(size);
	if (!data)
		return -1;

	memcpy(data, input, size);
	cert_parse((const char*) data, size, &certchain);
	free(data);

	return 0;
}
