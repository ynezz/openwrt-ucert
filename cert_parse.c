#include "cert_parse.h"

int cert_parse(const char *filebuf, ssize_t len, struct list_head *chain) {
	struct blob_attr *certtb[CERT_ATTR_MAX];
	struct blob_attr *bufpt;
	struct cert_object *cobj;
	const char *end;
	int ret = 1;

	end = filebuf + len;
	bufpt = (struct blob_attr *) filebuf;

	while (true) {
		len = end - (char *)bufpt;
		if (len <= 0)
			break;

		if (blob_parse_untrusted(bufpt, len, certtb, cert_policy, CERT_ATTR_MAX) <= 0)
			/* no attributes found */
			break;

		if (!certtb[CERT_ATTR_SIGNATURE])
			/* no signature -> drop */
			break;

		cobj = calloc(1, sizeof(*cobj));
		cobj->cert[CERT_ATTR_SIGNATURE] = blob_memdup(certtb[CERT_ATTR_SIGNATURE]);
		if (certtb[CERT_ATTR_PAYLOAD])
			cobj->cert[CERT_ATTR_PAYLOAD] = blob_memdup(certtb[CERT_ATTR_PAYLOAD]);

		list_add_tail(&cobj->list, chain);
		ret = 0;

		/* Repeat parsing while there is still enough remaining data in buffer
		 *
		 * Note that blob_next() is only valid for untrusted data because blob_parse_untrusted()
		 * verified that the buffer contains at least one blob, and that it is completely contained
		 * in the buffer */
		bufpt = blob_next(bufpt);
	}

	return ret;
}
