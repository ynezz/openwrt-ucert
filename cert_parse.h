#pragma once

#include <libubox/list.h>
#include <libubox/blobmsg.h>

/*
 * ucert structure
 * |               BLOB                    |
 * |    SIGNATURE    |       PAYLOAD       |
 * |                 |[ BLOBMSG CONTAINER ]|
 * |                 |[[T,i,v,e,f,pubkey ]]|
 */
enum cert_attr {
	CERT_ATTR_SIGNATURE,
	CERT_ATTR_PAYLOAD,
	CERT_ATTR_MAX
};

static const struct blob_attr_info cert_policy[CERT_ATTR_MAX] = {
	[CERT_ATTR_SIGNATURE] = { .type = BLOB_ATTR_BINARY },
	[CERT_ATTR_PAYLOAD] = { .type = BLOB_ATTR_NESTED },
};

/* list to store certificate chain at runtime */
struct cert_object {
	struct list_head list;
	struct blob_attr *cert[CERT_ATTR_MAX];
};

int cert_parse(const char *filebuf, ssize_t len, struct list_head *chain);
